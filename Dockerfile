FROM python:3
LABEL maintainer="peter.bourgonje@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y



## install prerequisites
RUN pip3 install langdetect Flask Flask-Cors
WORKDIR /broker

ADD languageIdentification.py /broker
ADD flaskController.py /broker

RUN export LC_ALL=C.UTF-8
RUN export LANG=C.UTF-8

EXPOSE 5000

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0
#CMD ["/bin/bash"]

