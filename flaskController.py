#!/usr/bin/python3
from flask import Flask, Response, flash, request, redirect, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename

# custom modules
import languageIdentification

"""
pip requirements:
langdetect


then to start run:
export FLASK_APP=main.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr



"""


app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)


li = languageIdentification.LanguageIdentifier()

@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"




##################### Language Identification #####################
@app.route('/detectLanguage', methods=['GET'])
def getLanguage():

    if request.args.get('input') == None:
        return 'Please provide some text as input.\n'

    lang = li.detect(request.args.get('input'))
    return lang

@app.route('/detectLanguages', methods=['GET'])
def getLanguages():

    if request.args.get('input') == None:
        return 'Please provide some text as input.\n'

    langs = li.detect_langs(request.args.get('input'))
    ld = {}
    for l in langs:
        ld[l.lang] = l.prob
    return json.dumps(ld)


##################### entity spotting #####################
@app.route('/', methods=['POST'])
def getLanguagesELG():
    if request.method == 'POST':
        ctype = request.headers["Content-Type"]
        accept = request.headers["Accept"]

        if not request.data:
            return create_error("elg.request.missing",
                                "Error: No request provided in message")

        request_body = request.data.decode('utf-8')
        request_dict = json.loads(request_body)
        mimeType = request_dict['mimeType']

        if mimeType == 'text/plain':
            content = request_dict['content']
        else:
            return create_error("elg.request.text.mimeType.unsupported",
                                "Error: MIME type {} not supported by this service".format(mimeType))
        print("***DEBUG***")
        print("Content: {}".format(content))
        print("Response type: {}".format(accept))
        
        if 'application/json' in accept:
            pass
        else:
            return create_error("elg.request.type.unsupported",
                                "ERROR: Request type {} not supported by this service".format(accept))

        if ctype == 'application/json':
            langs = li.detect_langs(content)
            ld = {}
            for l in langs:
                ld[l.lang] = l.prob
            response_dict = {
                "response": {
                    "type": "texts",
                    "texts": [{
                        "content": content,
                        "annotations": {
                            "Languages": json.dumps(ld)
                        }
                    }]
                }
            }
            return Response(response=json.dumps(response_dict), status=200, content_type='application/json')
        else:
            return create_error("elg.request.type.unsupported",
                                'Request type {} not supported by this service'.format(ctype))
    else:
        return create_error("elg.request.invalid",
                            'Invalid request message')

if __name__ == '__main__':

    port = int(os.environ.get('PORT',5000))
    app.run(host='localhost', port=port, debug=True)
