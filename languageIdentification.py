import langdetect

"""
Currently supported languages:
af, ar, bg, bn, ca, cs, cy, da, de, el, en, es, et, fa, fi, fr, gu, he, hi, hr, hu, id, it, ja, kn, ko, lt, lv, mk, ml, mr, ne, nl, no, pa, pl, pt, ro, ru, sk, sl, so, sq, sv, sw, ta, te, th, tl, tr, uk, ur, vi, zh-cn, zh-tw

To add new ones, see: https://pypi.org/project/langdetect/

"""

class LanguageIdentifier():

    def __init__(self):
        pass

    def detect(self, txt):

        lang = langdetect.detect(txt)
        return lang

    def detect_langs(self, txt):

        langdict = langdetect.detect_langs(txt)
        return langdict
